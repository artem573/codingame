import sys
import math

class Pos:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def right(self):
        if(self.x + 1 < width): return Pos(self.x + 1, self.y)
    
    def left(self):
        if(self.x - 1 >= 0): return Pos(self.x - 1, self.y)

    def bottom(self):
        if(self.y + 1 < height): return Pos(self.x, self.y + 1)

    def top(self):
        if(self.y - 1 >= 0): return Pos(self.x, self.y - 1)

    def neighbours(self):
        temp = []
        if(self.right()): temp.append(self.right())
        if(self.left()): temp.append(self.left())
        if(self.top()): temp.append(self.top())
        if(self.bottom()): temp.append(self.bottom())
        temp.append(self)
        return temp

    def nearby_walls(self):
        nbr_walls = 0
        nbrs = self.neighbours()
        for nbr in nbrs:
            if(field[nbr.x][nbr.y] == "#"):
                nbr_walls += 1
        return nbr_walls
        

class Wanderer:
    def __init__(self, id, pos, target):
        self.id = id
        self.pos = pos
        self.target = target

class Slasher:
    def __init__(self, id, pos, state, time_before_chang_state):
        self.id = id
        self.pos = pos
        self.state = state
        self.time_before_chang_state = time_before_chang_state

class Explorer:
    def __init__(self, id, pos, sanity):
        self.id = id
        self.pos = pos
        self.sanity = sanity

class Shelter:
    def __init__(self, id, pos, remaining_energy):
        self.id = id
        self.pos = pos
        self.remaining_energy = remaining_energy
           
class Spawn:
    def __init__(self, pos):
        self.pos = pos

class Move:
    def __init__(self, pos, weight):
        self.pos = pos
        self.weight = weight
    
def global_index(pos):
    return pos.x + pos.y*width

def sqrt(x):
    return x**0.5

def sqr(x):
    return x*x

def distance_vector(pos1, pos2):
    return sqrt(sqr(pos1.x - pos2.x) + sqr(pos1.y - pos2.y))

def get_dist(pos1, pos2):
    return dist[ global_index(pos1) ][ global_index(pos2) ]

# Survive the wrath of Kutulu
# Coded fearlessly by JohnnyYuge & nmahoude (ok we might have been a bit scared by the old god...but don't say anything)

width = int(input())
height = int(input())

N_nodes = height*width
dist_longest = N_nodes
SMALL = 1e-4

temp = []
for i in range(height):
    line = input()
    temp.append(line)

field = []
for i in range(width):
    field.append([])
    for j in range(height):
        field[i].append(temp[j][i])

pos_node = []
for j in range(height):
    for i in range(width):
        pos_node.append(Pos(i,j))

dist = []
for i in range(N_nodes):
    dist.append([])
    for j in range(N_nodes):
        dist[i].append(dist_longest)

for i in range(N_nodes):
    if(field[pos_node[i].x][pos_node[i].y] != "#"):
        dist[i][i] = 0
        step = 0
        current_list = [i]
        while current_list:
            step += 1

            temp_list = []
            for j_index in current_list:
                nbr_list = pos_node[j_index].neighbours()
                for pos_nbr in nbr_list:
                    j_nbr_index = global_index(pos_nbr)
                    if((field[pos_nbr.x][pos_nbr.y] != "#") and (dist[i][j_nbr_index] == dist_longest)):
                        dist[i][j_nbr_index] = step
                        found_j_nbr_dubl = False
                        for temp in temp_list:
                            if(temp == j_nbr_index):
                                found_j_nbr_dubl = True
                                break
                        if(not found_j_nbr_dubl): temp_list.append(j_nbr_index)

            if(not temp_list): break
            else: current_list = temp_list
       
            
spawns = []     
for j in range(height):
    for i in range(width):
        if(field[i][j] == "w"):
            spawns.append( Spawn( Pos(i, j) ) )
        
# sanity_loss_lonely: how much sanity you lose every turn when alone, always 3 until wood 1
# sanity_loss_group: how much sanity you lose every turn when near another player, always 1 until wood 1
# wanderer_spawn_time: how many turns the wanderer take to spawn, always 3 until wood 1
# wanderer_life_time: how many turns the wanderer is on map after spawning, always 40 until wood 1

sanity_loss_lonely, sanity_loss_group, wanderer_spawn_time, wanderer_life_time = [int(i) for i in input().split()]


it = 0
light_left = 0
plans_left = 0
yell_left = 1
sanity_own = 250

# game loop
while True:
    it += 1
    #print("it = " + str(it), file=sys.stderr)

    wanderers = []
    slashers = []
    explorers = []
    shelters = []

    entity_count = int(input())  # the first given entity corresponds to your explorer
    for i in range(entity_count):
        entity_type, id, x, y, param_0, param_1, param_2 = input().split()
        id = int(id)
        x = int(x)
        y = int(y)
        param_0 = int(param_0)
        param_1 = int(param_1)
        param_2 = int(param_2)

        pos = Pos(x, y)

        if(i == 0):
            id_own = id
            pos_own = pos
            sanity_own = param_0
            plans_left = param_1
            light_left = param_2
           
        if(entity_type == "WANDERER"):
            wanderers.append( Wanderer(id, pos, param_2) )
        if(entity_type == "SLASHER"):
            slashers.append( Slasher(id, pos, param_1, param_0) )
        if(entity_type == "EXPLORER"):
            explorers.append( Explorer(id, pos, param_0) )
        if(entity_type == "EFFECT_SHELTER"):
            shelters.append( Shelter(id, pos, param_0) )
     
    
    # Calc avaliable moves
    avail_moves = []
    if(pos_own.x > 0 and pos_own.x < width and pos_own.y > 0 and pos_own.y < height):
        nbr_list = pos_own.neighbours()
        for pos in nbr_list:
            if(field[pos.x][pos.y] != "#"):
                avail_moves.append( Move(pos, 0.0) )


    dist_to_closest_wand = dist_longest
    dist_to_closest_wand_hunt_own = dist_longest
    shadow_pos_taken = False
    slasher_in_sight = False

    # Calc moving weights
    weight_wand = -20.0
    weight_wand_hunt_own = -25.0

    weight_explorer_from = -0.5 # 2.0
    weight_explorer_to = 10.0

    weight_shelter = 40.0

    weight_slasher = -20.0
    weight_slasher_dist = -2.0

    weight_shadow = 100.0
    
    weight_spawn = -1.0
    weight_corner = -1.0

    weight_nbr_pos = 0.5
    weight_dead_end = -20.0;
    
    # Find closest explorer and max explorers sanity
    dist_to_closest_explorer = dist_longest
    max_sanity = 0
    pos_closest_explorer = Pos(-1,-1)
    for explorer in explorers:
        if(max_sanity < explorer.sanity and explorer.id != id_own):
            max_sanity = explorer.sanity
        if(explorer.id != id_own):
            distance = get_dist(pos_own, explorer.pos)
            if(dist_to_closest_explorer > distance):
                dist_to_closest_explorer = distance
                pos_closest_explorer = explorer.pos

    # Find distance to closest corner
    corners = [Pos(0,0), Pos(width-1,0), Pos(0,height-1), Pos(width-1,height-1)]
    closest_corner = corners[0]
    closest_corner_dist = dist_longest
    for corner in corners:            
        if(distance_vector(corner, pos_own) < closest_corner_dist):
            closest_corner = corner
            closest_corner_dist = distance_vector(corner, pos_own) 
                    

    for move in avail_moves:

        # Force bot to go to route intersections
        nbrs = move.pos.neighbours()
        nbr_walls = 0
        for nbr in nbrs:
            if(field[nbr.x][nbr.y] != "#"): 
                move.weight += weight_nbr_pos
                nbr_nbrs = nbr.neighbours()
                for nbr_nbr in nbr_nbrs:
                    if(field[nbr_nbr.x][nbr_nbr.y] != "#"): 
                        move.weight += weight_nbr_pos
            else:
                nbr_walls += 1
        if(nbr_walls == 3): move.weight += weight_dead_end
        
        # Forbid moving to simple deadends
        if(move.pos.nearby_walls() == 2):
            deadend_route = [pos_own, move.pos]
            while(True):
                if(deadend_route[-1].nearby_walls() == 3):
                    move.weight += weight_dead_end
                    break
                if(deadend_route[-1].nearby_walls() <= 1):
                    break
                nbrs = deadend_route[-1].neighbours()
                for nbr in nbrs:
                    if(field[nbr.x][nbr.y] != "#" and not (nbr.x == deadend_route[-1].x and nbr.y == deadend_route[-1].y) and not (nbr.x == deadend_route[-2].x and nbr.y == deadend_route[-2].y)):
                        nextmove = nbr
                deadend_route.append(nextmove)
            

        
        
        for spawn in spawns:
            distance = get_dist(move.pos, spawn.pos)
            distance = max(distance, 1.0)
            move.weight += weight_spawn/distance

        closest_corner_dist_new = distance_vector(closest_corner, move.pos)
        move.weight += weight_corner / closest_corner_dist_new
            
        for wanderer in wanderers:
            distance = get_dist(move.pos, wanderer.pos)
            if(dist_to_closest_wand > distance):
                dist_to_closest_wand = distance
            if(dist_to_closest_wand_hunt_own > distance and wanderer.target == id_own):
                dist_to_closest_wand_hunt_own = distance
            distance = max(distance, 1.0)

            if(wanderer.target == id_own): move.weight += weight_wand_hunt_own/distance
            else: move.weight += weight_wand/distance

             
        for explorer in explorers:
            if(explorer.id != id_own):
                distance = get_dist(move.pos, explorer.pos)
                distance = max(distance, 3.0)
                if((sanity_loss_lonely - sanity_loss_group > 3) and (not (sanity_own < 60 and max_sanity < 20))):
                    if(distance < dist_to_closest_explorer):
                        move.weight += weight_explorer_to
                else: move.weight += weight_explorer_from / distance
            
            
        for slasher in slashers:
            dist_vec = distance_vector(slasher.pos, move.pos)
            in_sight = False
            dist_map = get_dist(slasher.pos, move.pos)
            if(abs(dist_map - dist_vec) < SMALL): in_sight = True
    
            factor = 1.0
            if(slasher.state == 0 or slasher.state == 1 or slasher.state == 4):
                factor /= slasher.time_before_chang_state
                
            if(in_sight): 
                slasher_in_sight = True
                distance = get_dist(move.pos, slasher.pos)
                distance = max(distance, 0.5)
                move.weight += weight_slasher_dist / distance
                move.weight += weight_slasher * factor
                    
                    
        if( (not slasher_in_sight) and (dist_to_closest_wand >= 3) ):
            for shelter in shelters:
                if(shelter.remaining_energy > 0):
                    distance = get_dist(move.pos, shelter.pos)
                    distance = max(distance, 1.0)
                    move.weight += weight_shelter / distance
            
           
        
    best_move = 0
    best_move_weight = -1000
    for move in avail_moves:
        print(str(move.pos.x) + " " + str(move.pos.y) + " " + str(move.weight), file=sys.stderr)
        if(best_move_weight < move.weight):
            best_move_weight = move.weight
            best_move = move

    if(best_move):
        action = "MOVE"
        if(best_move.pos.x == pos_own.x and best_move.pos.y == pos_own.y): action = "WAIT"

    if((not slasher_in_sight) and (dist_to_closest_wand >= 3) and (plans_left > 0) and (it > 3) and ((dist_to_closest_explorer > 2) or (sanity_own < 50))): action = "PLAN"
    #if((dist_to_closest_wand_hunt_own <= 4) and (dist_to_closest_explorer <= 4) and (light_left > 0)): action = "LIGHT"
    if(shadow_pos_taken and (it > 10) and dist_to_closest_wand <= 2):
        if(yell_left > 0):
            yell_left -= 1
            action = "YELL"
        else:
            if(dist_to_closest_wand < 5 and light_left > 0):
                action = "LIGHT"
                
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # MOVE <x> <y> | WAIT
    if(action == "WAIT"): print("WAIT")
    if(action == "YELL"): print("YELL")
    if(action == "PLAN"): print("PLAN")
    if(action == "LIGHT"): print("LIGHT")
    if(action == "MOVE"): print("MOVE " + str(best_move.pos.x) + " " + str(best_move.pos.y))
