import sys
import math
import copy

class Object:
    def __init__(self, x, y, n = None):
        self.x = x
        self.y = y
        self.n = n

    def __mul__(self, var):
        return Object(var*self.x, var*self.y)

    def __add__(self, obj2):
        return Object(self.x + obj2.x, self.y + obj2.y)


def check_move(move, ball_moves, ball):
    ball_temp = copy.copy(ball)
    ball_temp.x = ball.n*move[1].x + ball.x
    ball_temp.y = ball.n*move[1].y + ball.y
    ball_temp.n -= 1

    n = ball_temp.n
    x = ball_temp.x
    y = ball_temp.y
    
    if ((x < 0) or (y < 0) or (x >= width) or (y >= height)): return False
    if(((n > 0) and (map[x][y] == 'H')) or (not (map[x][y] == '.') and not ((n == 0) and (map[x][y] == 'H')))): return False
    
    ball_moves.append(move)
    
    if((n == 0) and (map[x][y] == 'H')): return True

    n_moves = 0
    for move in Moves:
        n_moves += 1
        if(check_move(move, ball_moves, ball_temp)): return True

    if(n_moves == len(Moves)):
        ball_moves.pop_back()
        return False

    
width, height = [int(i) for i in input().split()]

map_temp = []
balls = []
holes = []
hazards = []

for y in range(height):
    map_temp.append([])
    row = input()
    for row_i in row:
        map_temp[y].append(row_i)
 
map = []       
for x in range(width):
    map.append([])
    for y in range(height):
        map[x].append(map_temp[y][x])       

for x in range(width):
    for y in range(height):
        if(map[x][y] == 'H'): holes.append(Object(x,y))
        if(map[x][y] == 'X'): hazards.append(Object(x,y))
        if((map[x][y] != 'X') and (map[x][y] != 'H') and (map[x][y] != '.')): balls.append(Object(x,y,int(map[x][y])))

#print(map, file=sys.stderr)

Moves = [ ['LEFT', Object(-1,0), '<'], ['RIGHT', Object(1,0), '>'], ['UP', Object(0,-1), '^'], ['DOWN', Object(0,1), 'v'] ]

avail_moves = []
for ball in balls:
    ball_moves = []
    for move in Moves:
        if(check_move(move, ball_moves, ball)): break
    avail_moves.append(ball_moves)




res = copy.copy(map)
for i in range(len(balls)):
    move = avail_moves[i]
    ball = balls[i]
    index = 0
    while(ball.n > 0):
        m = ball.n
        while(m > 0):
            res[ball.x][ball.y] = move[index][2]
            ball.x += move[index][1].x
            ball.y += move[index][1].y
            m -= 1
            
        index += 1
        ball.n -= 1
    res[ball.x][ball.y] = 'H'
	
for x in range(width):
    for y in range(height):
        arrow = False
        for move in Moves:
            if(res[x][y] == move[2]):
                arrow = True
                break
        if(not arrow): res[x][y] = '.'

#print(res, file=sys.stderr)

for y in range(height):
	temp = ''
	for x in range(width):
		temp += res[x][y]
	print(temp)
