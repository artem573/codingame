#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stack>

struct Motorbike
{
  int y;
  int a;
};

struct MB_system
{
  int speed;
  int alive;
  int x;
  std::vector < Motorbike > mbs;
};

const int N_lines = 4;
const int S_max = 49;
const int S_min = 1;
const int LARGE = 1000;	
enum Action { WAIT, JUMP, SPEED, SLOW, UP, DOWN };

bool check_move(const Action action, const MB_system & mb_system)
{
  if ( (action == Action::SLOW)  && (mb_system.speed <= S_min) ){ return false; }
  if ( (action == Action::SPEED) && (mb_system.speed >= S_max) ){ return false; }
  if (action == Action::UP)
  {
    for (auto & mb_i : mb_system.mbs)
    {
      if((mb_i.y == 0) && (mb_i.a == 1)) { return false; } 
    }
  }
  if (action == Action::DOWN)
  {		
    for (auto & mb_i : mb_system.mbs)
    {
      if((mb_i.y == N_lines-1) && (mb_i.a == 1)) { return false; } 
    }
  }	
	
  if ( (action != Action::SPEED) && (mb_system.speed <= 0) ){ return false; }
	
  return true;
}

MB_system make_move(const std::vector< std::string > & map, const Action action, const MB_system & mb_system)
{
  MB_system mb_system_new = mb_system;
  int dy = 0;
	
  if (action == Action::SLOW){ mb_system_new.speed -= 1; }
  if (action == Action::SPEED){ mb_system_new.speed += 1; }
  if (action == Action::UP){ dy = -1; }
  if (action == Action::DOWN){ dy = 1; }
		
  mb_system_new.x += mb_system_new.speed;	
		
  for (auto & mb_i : mb_system_new.mbs)
  {
    if(mb_i.a == 1)
    {
      std::string check_str = "";

      unsigned int x0 = mb_system.x + 1;
      unsigned int len = mb_system_new.speed - 1;

      if(x0 >= map[0].size()){ x0 = map[0].size() - 1; }
      if(x0 + len >= map[0].size()){ len = map[0].size() - x0 - 1; }
        

      if((action != Action::JUMP) && (mb_system_new.speed > 1))
      {
        check_str += map[mb_i.y+dy].substr(x0, len);
        check_str += map[mb_i.y].substr(x0, len);
      }
			
      mb_i.y += dy;		
      check_str += map[mb_i.y][mb_system_new.x];
		
      if(check_str.find('0') != std::string::npos)
      { 
        mb_i.a = 0;
        mb_system_new.alive -= 1;
      }
    }
  }
			
  return mb_system_new;
}


int main()
{
  std::vector< Action > actions { Action::SPEED, Action::WAIT, Action::JUMP, Action::UP, Action::DOWN, Action::SLOW };
	
	
  int M; // the amount of motorbikes to control
  std::cin >> M; std::cin.ignore();
  int V; // the minimum amount of motorbikes that must survive
  std::cin >> V; std::cin.ignore();
	
  std::vector< std::string > map(N_lines);
  std::string line; 
  for(int i = 0; i < N_lines; ++i)
  {
    std::cin >> line; std::cin.ignore();
    map[i] = line;
  }

  MB_system mb_system;
  mb_system.mbs.resize(M);
	
  const int depth_search = 50;
		
  // game loop
  while (1) {
    std::cin >> mb_system.speed; std::cin.ignore();
		
    mb_system.alive = 0;
    for (int i = 0; i < M; i++) 
    {
      std::cin >> mb_system.x >> mb_system.mbs[i].y >> mb_system.mbs[i].a; std::cin.ignore();
      mb_system.alive += mb_system.mbs[i].a;
    }
		
    std::stack< MB_system > MB_state_stack;
    std::stack< unsigned int > action_stack;
		
    MB_state_stack.push(mb_system);
    action_stack.push(0);
		
    Action good_move = actions[0];
		
    while(true)
    {
      mb_system = MB_state_stack.top();
          
      if((MB_state_stack.size() > depth_search) || (mb_system.x >= map[0].size()))
      {
          std::cerr << MB_state_stack.size() << std::endl;
          break; 
      }
		    
      const unsigned int move_i_0 = action_stack.top();
      for (unsigned int move_i = move_i_0; move_i < actions.size(); ++move_i)
      {
        action_stack.pop();
        action_stack.push(move_i);
        if(action_stack.size() == 1){ good_move = actions[move_i]; }
                
        if(check_move(actions[move_i], mb_system))
        {
          MB_system mb_system_new = make_move(map, actions[move_i], mb_system);
          if (mb_system_new.alive >= V)
          {
            action_stack.push(0);
            MB_state_stack.push(mb_system_new);
            break;
          }					
        }
      }
			
      if(action_stack.top() >= actions.size()-1)
      {
        if(action_stack.size() == 1){ break; }
        action_stack.pop();
        MB_state_stack.pop();

        int temp = action_stack.top();
        action_stack.pop();
        action_stack.push(temp+1);
        mb_system = MB_state_stack.top();
      }
    }
			
    switch(good_move)
    {
    case Action::WAIT:
    {
      std::cout << "WAIT" << std::endl;
      break;
    }
    case Action::JUMP:
    {
      std::cout << "JUMP" << std::endl;
      break;
    }
    case Action::SLOW:
    {
      std::cout << "SLOW" << std::endl;
      break;
    }
    case Action::SPEED:
    {
      std::cout << "SPEED" << std::endl;
      break;
    }
    case Action::UP:
    {
      std::cout << "UP" << std::endl;
      break;
    }
    case Action::DOWN:
    {
      std::cout << "DOWN" << std::endl;
      break;
    }	
    }			
  }
	
  return 0;
}
