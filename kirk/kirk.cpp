#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

const int LARGE = 10000;

enum Direction { RIGHT, LEFT, UP, DOWN };
enum Action { EXPLORING, HEADING, RETURNING };


struct Pos
{
  int x;
  int y;
  
  Pos(int x, int y): x(x), y(y) {}
  Pos(): x(-1), y(-1) {}
  Pos(const Pos & pos): x(pos.x), y(pos.y) {}
  
  Pos left() const
  {
    return Pos(x-1,y);
  }

  Pos right() const
  {
    return Pos(x+1,y);
  }

  Pos up() const
  {
    return Pos(x,y-1);
  }

  Pos down() const
  {
    return Pos(x,y+1);
  }


  Pos newPos(const Direction & dir) const
  {
    Pos pos;
    
    switch (dir) {
    case RIGHT:
      pos = this->right();
      break;
    case LEFT:
      pos = this->left();
      break;
    case UP:
      pos = this->up();
      break;
    case DOWN:
      pos = this->down();
      break;
    }

    return pos;
  }

  Direction getDir(const Pos & pos) const
  {
    Direction dir;
    if(pos.x == this->x + 1)
    {
      dir = RIGHT;
    }
    if(pos.x == this->x - 1)
    {
      dir = LEFT;
    }
    if(pos.y == this->y + 1)
    {
      dir = DOWN;
    }
    if(pos.y == this->y - 1)
    {
      dir = UP;
    }

    return dir;

  }
  
  std::vector<Pos> neighbours(const std::vector< std::vector<char> > & map) const
  {
    const Pos temp[] = { this->left(), this->right(), this->up(), this->down() };
    std::vector<Pos> ret;
    for (size_t i = 0; i < sizeof(temp) / sizeof(temp[0]); ++i)
    {
      if((map[temp[i].x][temp[i].y] != '#') && (map[temp[i].x][temp[i].y] != '?'))
      {
        ret.push_back(temp[i]);
      }
    }
    
    return ret;
  }
};


Direction rotateCW(const Direction & dir)
{
  Direction new_dir = dir;

  switch (dir)
  {
  case RIGHT:
  {
    new_dir = DOWN;
    break;
  }
  case LEFT:
  {
    new_dir = UP;
    break;
  }
  case DOWN:
  {
    new_dir = LEFT;
    break;
  }
  case UP:
  {
    new_dir = RIGHT;
    break;
  }
  }

  return new_dir;
}

Direction rotateCCW(const Direction & dir)
{
  Direction new_dir = dir;

  switch (dir)
  {    
  case RIGHT:
  {
    new_dir = UP;
    break;
  }
  case LEFT:
  {
    new_dir = DOWN;
    break;
  }
  case DOWN:
  {
    new_dir = RIGHT;
    break;
  }
  case UP:
  {
    new_dir = LEFT;
    break;
  }
  }

  return new_dir;
}

std::vector<Pos> getPath(const std::vector< std::vector<char> > & map, const Pos & pos1, const Pos & pos2)
{
  int NX = map.size();
  int NY = map[0].size();

  std::vector< std::vector<int> > dist(NX, std::vector<int>(NY, LARGE));
  std::vector< std::vector<Pos> > parent(NX, std::vector<Pos>(NY, pos1));
  std::vector<Pos> stack_parent;
  stack_parent.push_back(pos1);
  std::vector<Pos> stack_child;
  dist[pos1.x][pos1.y] = 0;

  bool found_pos2 = false;
  while((stack_parent.size() > 0) && (not found_pos2) )
  {
    for(const auto & it_parent : stack_parent)
    {
      std::vector<Pos> parent_nbr = it_parent.neighbours(map);
      
      for(const auto & it_parent_nbr: parent_nbr)
      {
        if(dist[it_parent_nbr.x][it_parent_nbr.y] == LARGE)
        {
          stack_child.push_back(it_parent_nbr);
          dist[it_parent_nbr.x][it_parent_nbr.y] = dist[it_parent.x][it_parent.y] + 1;
          parent[it_parent_nbr.x][it_parent_nbr.y] = Pos(it_parent.x, it_parent.y);
        }

        if( (it_parent_nbr.x == pos2.x) && (it_parent_nbr.y == pos2.y) )
        {
          found_pos2 = true;
        }
      }
    }

    stack_parent = stack_child;
    stack_child.clear();
  }

  std::vector<Pos> path;

  if(found_pos2)
  {
    Pos cur_pos = pos2;
    path.push_back(cur_pos);
    while(true)
    {
      cur_pos = parent[cur_pos.x][cur_pos.y];
      if( (cur_pos.x == pos1.x) && (cur_pos.y == pos1.y) )
      {
        break;
      }
      path.push_back(cur_pos);
    }
  }
  
  return path;
}


bool freeDir(const std::vector< std::vector<char> > & map, const Pos & pos, const Direction & dir)
{
  Pos next_pos = pos.newPos(dir); 
  
  if(map[next_pos.x][next_pos.y] != '#')
  {
    return true;
  }
  else
  {
    return false;
  }
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
  int NY; // number of rows.
  int NX; // number of columns.
  int A; // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
  std::cin >> NY >> NX >> A; std::cin.ignore();
  
  std::vector< std::vector< char > > map(NX, std::vector<char>(NY,'?'));
  std::vector< std::vector< bool > > visited(NX, std::vector<bool>(NY,false));
  
  Direction dir = RIGHT;

  int it = 0;
  Pos pos_T;
  Pos pos_C;
  Action action = EXPLORING;
  bool found_C = false;

  std::vector<Pos> path;
  std::vector<Pos> junctions;
  int not_explored_junctions = 0;

  // game loop
  while (1)
  {
    it ++;

    int KY; // row where Kirk is located.
    int KX; // column where Kirk is located.
    std::cin >> KY >> KX; std::cin.ignore();

    for (int j = 0; j < NY; ++j)
    {
      std::string ROW; // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).
      std::cin >> ROW; std::cin.ignore();
      
      for (int i = 0; i < NX; ++i)
      {
        map[i][j] = ROW[i];

        if( (it == 1) && (ROW[i] == 'T') )
        {
          pos_T = Pos(i,j);
        }
        
        if( (ROW[i] == 'C') && (not found_C) )
        {
          pos_C = Pos(i,j);
          found_C = true;
        }
      }
    }

    Pos pos_K(KX, KY);

    if( (found_C) && (action == EXPLORING) )
    {
      path = getPath(map, pos_K, pos_C);
      if(path.size() > 0)
      {
        action = HEADING;
      }
    }

    if( (pos_K.x == pos_C.x) && (pos_K.y == pos_C.y) )
    {
      path = getPath(map, pos_K, pos_T);
      action = RETURNING;
    }

    visited[pos_K.x][pos_K.y] = true;
    std::vector<Pos> pos_nbr = pos_K.neighbours(map);
    if( pos_nbr.size() > 2 )
    {
      junctions.push_back(pos_K);
      not_explored_junctions += 1;
    }
    

    switch (action)
    {
    case EXPLORING:
    {
      if ( freeDir(map, pos_K, rotateCW(dir)) )
      {
        dir = rotateCW(dir);
      }
      else
      {
        while( not freeDir(map, pos_K, dir) )
        {
          dir = rotateCCW(dir);
        }
      }
      break;
    }
    case HEADING:
    {
      Pos next_pos = path.back();
      path.pop_back();
      dir = pos_K.getDir(next_pos);
      break;
    }
    case RETURNING:
    {
      Pos next_pos = path.back();
      path.pop_back();
      dir = pos_K.getDir(next_pos);
      break;
    }
    }
    
    
    std::string move;
    switch (dir)
    {
    case LEFT:
    {
      move = "LEFT";
      break;
    }
    case RIGHT:
    {
      move = "RIGHT";
      break;
    }
    case UP:
    {
      move = "UP";
      break;
    }
    case DOWN:
    {
      move = "DOWN";
      break;
    }
    }
    
    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;
    std::cout << move << std::endl; // Kirk's next move (UP DOWN LEFT or RIGHT).
    }
  
  return 0;
}

