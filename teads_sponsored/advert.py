import sys
import math
from collections import deque

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

nbrs = []

n = int(input())  # the number of adjacency relations
print(n, file=sys.stderr)
for i in range(n):
	xi, yi = [int(j) for j in input().split()]
	while len(nbrs) <= xi:
		nbrs.append([])
	nbrs[xi].append(yi)
	while len(nbrs) <= yi:
		nbrs.append([])
	nbrs[yi].append(xi)
	print(xi, yi, file=sys.stderr)

social = ["x" for i in range(len(nbrs))]
depth = 0
queue = deque([])

for i in range(len(nbrs)):
	#print(i, nbrs[i], file=sys.stderr)
	if(len(nbrs[i]) == 1):
		social[i] = depth
		queue.append(i)

queue.append("q")
depth += 1
n_left = 0

while(len(queue) > 1):
	#print(social, file=sys.stderr)
	#print(queue[0], file=sys.stderr)
	if(queue[0] == "q"):
		queue.popleft()
		queue.append("q")
		depth += 1
		n_left = len(queue)-1
		
	index = queue[0]
	#print(index, file=sys.stderr)
	queue.popleft()
	for nbrs_i in nbrs[index]:
		#print(index, nbrs_i, social[nbrs_i], file=sys.stderr)
		if(social[nbrs_i] == "x"):
			social[nbrs_i] = depth
			queue.append(nbrs_i)
			
max_depth = 0
for social_i in social:
    if(social_i != "x"):               
        if(max_depth < social_i): max_depth = social_i
        
if(n_left > 1):
    max_depth += 1
# The minimal amount of steps required to completely propagate the advertisement
print(max_depth)
