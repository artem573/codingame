import sys
import math

kmh_to_ms_r = 3.6
min_speed_kmh = 1
max_speed_kmh = 200

distance = []
duration = []

allowed_speed_kmh = int(input()) # [km/h]

speed_list_kmh = [i for i in range(allowed_speed_kmh, min_speed_kmh-1, -1)]

light_count = int(input())

for i in range(light_count):
    a, b = [int(j) for j in input().split()]
    distance.append(a) # [m]
    duration.append(b)

for speed_i in speed_list_kmh:
    speed = speed_i
    found = True
    for i in range(light_count):
        if((duration[i] - (distance[i]*kmh_to_ms_r/speed_i) % (2*duration[i])) <= 0):
            found = False
            break
    if(found): break

            
print(speed)
